import static org.junit.Assert.*;
import org.junit.Test;

/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {

   @Test (timeout=20000)
   public void test1() { 
      GraphTask.main (null);
      assertTrue ("There are no tests", true);
   }


   @Test (timeout = 20000)
   public void test_path_finding_1() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("Test 1");

      GraphTask.Vertex f = task.new Vertex("F", null);
      f.height = 2;
      GraphTask.Vertex e = task.new Vertex("E", f);
      e.height = 3;
      GraphTask.Vertex d = task.new Vertex("D", e);
      d.height = 8;
      GraphTask.Vertex c = task.new Vertex("C", d);
      c.height = 7;
      GraphTask.Vertex b = task.new Vertex("B", c);
      b.height = 4;
      GraphTask.Vertex a = task.new Vertex("A", b);
      a.height = 0;

      GraphTask.Arc AB = task.new Arc("AB", b);
      GraphTask.Arc AC = task.new Arc("AC", c);
      a.first = AB;
      AB.next = AC;

      GraphTask.Arc BD = task.new Arc("BD", d);
      GraphTask.Arc BA = task.new Arc("BA", a);
      b.first = BD;
      BD.next = BA;

      GraphTask.Arc CE = task.new Arc("CE", e);
      GraphTask.Arc CA = task.new Arc("CA", a);
      c.first = CE;
      CE.next = CA;

      GraphTask.Arc DF = task.new Arc("DF", f);
      d.first = DF;

      GraphTask.Arc EF = task.new Arc("EF", f);
      GraphTask.Arc EC = task.new Arc("EC", c);
      e.first = EF;
      EF.next = EC;

      g.first = a;

      String result = g.setPathToVertex(f);

      assertEquals("A->C->E->F", result);
      System.out.println (g.toString());
      System.out.println (result);
   }

   @Test (timeout = 20000)
   public void test_path_finding_2() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("Test 2");

      GraphTask.Vertex d = task.new Vertex("D", null);
      d.height = 3;
      GraphTask.Vertex c = task.new Vertex("C", d);
      c.height = 2;
      GraphTask.Vertex b = task.new Vertex("B", c);
      b.height = 4;
      GraphTask.Vertex a = task.new Vertex("A", b);
      a.height = 0;

      GraphTask.Arc AB = task.new Arc("AB", b);
      GraphTask.Arc AD = task.new Arc("AD", d);
      a.first = AB;
      AB.next = AD;

      GraphTask.Arc BC = task.new Arc("BC", c);
      GraphTask.Arc BA = task.new Arc("BA", a);
      b.first = BC;
      BC.next = BA;

      GraphTask.Arc DC = task.new Arc("DC", c);
      GraphTask.Arc DA = task.new Arc("DA", a);
      d.first = DC;
      DC.next = DA;

      g.first = a;

      String result = g.setPathToVertex(c);

      assertEquals("A->D->C", result);
      System.out.println (g.toString());
      System.out.println (result);
   }

   @Test (timeout = 20000)
   public void test_path_finding_3() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("Test 3");

      GraphTask.Vertex f = task.new Vertex("F", null);
      f.height = 2;
      GraphTask.Vertex e = task.new Vertex("E", f);
      e.height = 4;
      GraphTask.Vertex d = task.new Vertex("D", e);
      d.height = 2;
      GraphTask.Vertex c = task.new Vertex("C", d);
      c.height = 8;
      GraphTask.Vertex b = task.new Vertex("B", c);
      b.height = 4;
      GraphTask.Vertex a = task.new Vertex("A", b);
      a.height = 0;

      GraphTask.Arc AB = task.new Arc("AB", b);
      a.first = AB;

      GraphTask.Arc BC = task.new Arc("BC", c);
      GraphTask.Arc BD = task.new Arc("BD", d);
      b.first = BC;
      BC.next = BD;

      GraphTask.Arc CE = task.new Arc("CE", d);
      c.first = CE;

      GraphTask.Arc DE = task.new Arc("DE", e);
      d.first = DE;

      GraphTask.Arc EF = task.new Arc("EF", f);
      e.first = EF;

      g.first = a;

      String result = g.setPathToVertex(f);

      assertEquals("A->B->D->E->F", result);
      System.out.println (g.toString());
      System.out.println (result);
   }

   @Test (timeout = 20000)
   public void test_path_finding_4() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("Test 4");

      GraphTask.Vertex f = task.new Vertex("F", null);
      f.height = 2;
      GraphTask.Vertex e = task.new Vertex("E", f);
      e.height = 1;
      GraphTask.Vertex d = task.new Vertex("D", e);
      d.height = 2;
      GraphTask.Vertex c = task.new Vertex("C", d);
      c.height = 4;
      GraphTask.Vertex b = task.new Vertex("B", c);
      b.height = 3;
      GraphTask.Vertex a = task.new Vertex("A", b);
      a.height = 2;

      GraphTask.Arc AB = task.new Arc("AB", b);
      GraphTask.Arc AE = task.new Arc("AE", e);
      a.first = AB;
      AB.next = AE;

      GraphTask.Arc BC = task.new Arc("BC", c);
      GraphTask.Arc BD = task.new Arc("BD", d);
      GraphTask.Arc BE = task.new Arc("BE", e);
      b.first = BC;
      BC.next = BD;
      BD.next = BE;

      GraphTask.Arc CD = task.new Arc("CD", d);
      GraphTask.Arc CB = task.new Arc("CB", b);
      c.first = CD;
      CD.next = CB;

      GraphTask.Arc DE = task.new Arc("DE", e);
      GraphTask.Arc DF = task.new Arc("DF", f);
      d.first = DE;
      DE.next = DF;

      GraphTask.Arc EB = task.new Arc("EB", b);
      GraphTask.Arc ED = task.new Arc("ED", d);
      e.first = EB;
      EB.next = ED;

      g.first = a;

      String result = g.setPathToVertex(f);

      assertEquals("A->E->D->F", result);
      System.out.println (g.toString());
      System.out.println (result);
   }

   @Test (timeout = 20000)
   public void test_path_finding_5() {
      GraphTask task = new GraphTask();
      GraphTask.Graph g = task.new Graph("Test 5");

      GraphTask.Vertex c = task.new Vertex("C", null);
      c.height = 7;
      GraphTask.Vertex b = task.new Vertex("B", c);
      b.height = 4;
      GraphTask.Vertex a = task.new Vertex("A", b);
      a.height = 0;

      GraphTask.Arc AB = task.new Arc("AB", b);
      GraphTask.Arc AC = task.new Arc("AC", c);
      a.first = AB;
      AB.next = AC;

      GraphTask.Arc BC = task.new Arc("BC", c);
      GraphTask.Arc BA = task.new Arc("BA", a);
      b.first = BC;
      BC.next = BA;

      GraphTask.Arc CB = task.new Arc("CB", b);
      GraphTask.Arc CA = task.new Arc("CA", a);
      c.first = CB;
      CB.next = CA;

      g.first = a;

      String result = g.setPathToVertex(c);

      assertEquals("A->C", result);
      System.out.println (g.toString());
      System.out.println (result);
   }
}

